FROM golang:latest

RUN go build ./goweb

EXPOSE 8001

ENTRYPOINT ["./main"]
